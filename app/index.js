import React, { Component, AsyncStorage } from 'react';
import { StyleSheet,  Text, View } from 'react-native';
import { Router, Scene } from 'react-native-router-flux';
import { connect, Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';

import hooks from 'feathers-hooks';
import feathers from 'feathers/client';
import socketio from 'feathers-socketio/client';
import localstorage from 'feathers-localstorage';
import authentication from 'feathers-authentication-client';

import LaunchScreen from './LaunchScreen';
import FoodListView from './FoodListView';
import OrderListView from './OrderListView';
import ProfileListView from './ProfileListView';
import ScarletScreen from './ScarletScreen';
import GrayScreen from './GrayScreen';

const RouterWithRedux = connect()(Router);
import reducers from '../reducers';
const middleware = [];
const store = compose(
  applyMiddleware(...middleware)
)(createStore)(reducers);

if (window.navigator && Object.keys(window.navigator).length == 0) {
    window = Object.assign(window, {navigator: {userAgent: 'ReactNative'}});
}

var io = require('socket.io-client');

const TabIcon = ({ selected, title }) => {
  return (
    <Text style={{color: selected ? 'red' :'black'}}>{title}</Text>
  );
}

class HomeChef extends Component {

  constructor(props) {
    super(props);
    this.state = {
      connected: false,
      // dataSource: ds.cloneWithRows(['row 1', 'row 2'])
    };
    this.socket = io('http://192.168.2.25:3030', {transports: ['websocket'], forceNew: true});
    this.app = feathers()
        .configure(socketio(this.socket))
        .configure(hooks())
        .configure(authentication({ storage: AsyncStorage }));
    this.doAuthentication = function () {
      this.app.authenticate()
              .then( (result) => {
                  console.log('result:', result);
                  return this.app.passport.verifyJWT(result.accessToken);
              }).then( (payload) => {
                  console.log('payload:', payload);
                  // this.app.service('foods').find().then(result => {console.log('Foods result:', result);});
                  return this.app.service('users').get(payload.userId);  // My jwtoken contains the user's id
              }).then ( (user) => {
                  this.app.set('user', user);
                  renderApp(user);     // Start up the application proper
              }).catch(function(error){
                  console.log("jwt Auth failed", error);
                  // renderLogin();  // Show the login form
              });
    }

    // const foodDs = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

  }

  componentDidMount() {

    console.log('1', store.getState());
    console.log('store: ', store);

    let authtokenopt = {
      strategy: 'jwt',
      accessToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6ImFjY2VzcyJ9.eyJpYXQiOjE0ODQzMjE4MzYsImV4cCI6MTQ4NDQwODIzNiwiYXVkIjoiaHR0cHM6Ly95b3VyZG9tYWluLmNvbSIsImlzcyI6ImZlYXRoZXJzIiwic3ViIjoiYW5vbnltb3VzIn0.LfUpLZPBNp0vQxX8-elxqd_xe9m7V6YO-Bl7vSsDPvI'
    }
    let authlocalopt = {
      strategy: 'local',
      email: 'a@a.aa',
      password: 'a'
    }

    this.doAuthentication();

    this.app.authenticate({
      strategy: 'local',
      email: 'a@a.aa',
      password: 'a'
    }).then( (response) => {
        // Local authentication (username/password) succeeded.
        //  We now have a JWToken, which has been stashed in
        //  localstorage
        console.log('response from local auth:', response);
        this.doAuthentication();  // The function from the previous post
    }).catch( (error) => {
        console.error('Error authenticating local auth!', error);
        // re-render form
    });

    // this.socket.io.engine.on('upgrade', function(transport) {
    //   console.log('transport changed');
    //   app.authenticate();
    // });

    this.socket.on('connect', () => {
        console.log('connect');
    });
    this.socket.on('connection_failed', () => {
        console.log('connection_failed');
    });
    this.socket.on('test', (data) => {
        console.log('Received data:', data);
    });


    // var intervalID = window.setInterval(() => {
    //   this.socket.emit('foods::find', {}, (error, data) => {
    //     console.log('Found all food', data);
    //     console.log('Error: ', error);
    //   });
    // }, 5000);



    // Get the message service that uses a websocket connection
    const foodService = this.app.service('foods');
    foodService.on('created', food => console.log('Someone created a food', food));
  }


  render() {
    return (
      <Provider store={store}>
      <RouterWithRedux>
        <Scene key="root">


          <Scene
            key="tabbar"
            tabs={true}
            tabBarStyle={{ backgroundColor: '#FFFFFF', height: 60 }} >

            <Scene key="food" title="Food" icon={TabIcon}>
              <Scene
                key="FoodListView"
                component={FoodListView}
                title="Food Page 1"
              />
            </Scene>

            <Scene key="order" title="Order" icon={TabIcon}>
              <Scene
                key="OrderListView"
                component={OrderListView}
                title="Order Page 1"
              />
            </Scene>

            <Scene key="profile" title="Profile" icon={TabIcon}>
              <Scene
                key="ProfileListView"
                component={ProfileListView}
                title="Profile"
              />
            </Scene>
          </Scene>


        </Scene>
      </RouterWithRedux>
      </Provider>
    );
  }
}

export default HomeChef;
